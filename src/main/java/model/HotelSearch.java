package model;

import java.sql.Timestamp;
import java.util.Optional;

public class HotelSearch {
    Optional<String> cookie;
    Timestamp date;
    Optional<String> sid;
    Timestamp checkIn;
    Timestamp checkOut;
    Optional<String> city;

    public Optional<String> getCookie() {
        return cookie;
    }

    public void setCookie(Optional<String> cookie) {
        this.cookie = cookie;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Optional<String> getSid() {
        return sid;
    }

    public void setSid(Optional<String> sid) {
        this.sid = sid;
    }

    public Timestamp getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Timestamp checkIn) {
        this.checkIn = checkIn;
    }

    public Timestamp getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Timestamp checkOut) {
        this.checkOut = checkOut;
    }

    public Optional<String> getCity() {
        return city;
    }

    public void setCity(Optional<String> city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "HotelSearch{" +
                "cookie=" + cookie +
                ", date=" + date +
                ", sid=" + sid +
                ", checkIn=" + checkIn +
                ", checkOut=" + checkOut +
                ", city=" + city +
                '}';
    }
}
