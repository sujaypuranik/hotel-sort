import bolt.CassandraBolt;
import bolt.HotelBolt;
import kafka.api.OffsetRequest;
import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.kafka.KafkaSpout;
import org.apache.storm.kafka.SpoutConfig;
import org.apache.storm.kafka.ZkHosts;
import org.apache.storm.topology.TopologyBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.HotelSortConstants;
import util.PropertiesReader;

import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

public class HotelTopology implements Serializable {
    private static final Logger LOGGER = LoggerFactory.getLogger(HotelTopology.class);

    public static void main(String[] args) throws InvalidTopologyException, AuthorizationException,
            AlreadyAliveException, IOException {
        Properties properties = PropertiesReader.getProperties(args[0]);

        String zookeeperBrokerAddress = properties.getProperty("zookeeper-host");
        String zookeeperBrokerPath = properties.getProperty("zookeeper-path");
        String zookeeperRoot = properties.getProperty("zookeeper-root");
        String topic = properties.getProperty("topic");
        String groupid = properties.getProperty("group-id");
        String topologyName = properties.getProperty("topology-name");
        String cassandraAddress = properties.getProperty("cassandra-host");

        ZkHosts zkHosts = new ZkHosts(zookeeperBrokerAddress, zookeeperBrokerPath);
        TopologyBuilder topologyBuilder = new TopologyBuilder();
        SpoutConfig spoutConfig = new SpoutConfig(zkHosts, topic, zookeeperRoot, groupid);
        spoutConfig.startOffsetTime = OffsetRequest.LatestTime();

        topologyBuilder.setSpout("hotel-spout", new KafkaSpout(spoutConfig), 1);
        topologyBuilder.setBolt("hotel-bolt", new HotelBolt(), 1)
                .shuffleGrouping("hotel-spout");

        topologyBuilder.setBolt("cassandra-bolt", new CassandraBolt(cassandraAddress), 1)
                .shuffleGrouping("hotel-bolt", HotelSortConstants.SEARCH_STREAM_SOURCE_ID)
                .shuffleGrouping("hotel-bolt", HotelSortConstants.BOOK_STREAM_SOURCE_ID);

        LOGGER.info("Trying to build and submit the topology.");
        StormSubmitter.submitTopology(topologyName, new Config(), topologyBuilder.createTopology());
    }
}
