package util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;

public class PropertiesReader {
    public static Properties getProperties(final String environment) throws IOException {
        Properties properties = new Properties();
        properties.load(new InputStreamReader(Objects.requireNonNull(PropertiesReader.class.getClassLoader().
                getResourceAsStream("config-" + environment + ".properties"),
                "Could not load properties.")));
        return properties;
    }
}
