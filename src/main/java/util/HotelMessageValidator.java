package util;

import com.cleartrip.analytics.Header;
import com.cleartrip.analytics.Hotels;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.netty.util.internal.StringUtil.isNullOrEmpty;

public class HotelMessageValidator {
    static final Logger LOGGER = LoggerFactory.getLogger(Hotels.HotelMessage.class);
    public static boolean isValid(final Hotels.HotelMessage hotelMessage) {
        if (hotelMessage == null) {
            LOGGER.info("Hotelmessage is null");
            return false;
        }
        Header.LogHeader header = hotelMessage.getHeader();
        if (header == null) {
            LOGGER.info("header is null");
            return false;
        }
        /*if (isNullOrEmpty(header.getUserCookie()) || header.getChannel() == null) {
            LOGGER.info("isNullOrEmpty(header.getUserCookie()) || header.getChannel() == null is false");
            return false;
        }
        if(header.getChannel()!= Header.Channel.CHANNEL_MOBILE && header.getChannel() != Header.Channel.CHANNEL_SITE) {
            LOGGER.info("header.getChannel()!= Header.Channel.CHANNEL_MOBILE && header.getChannel() != Header.Channel.CHANNEL_SITE is false");
            return false;
        }*/
        return true;
    }
}
