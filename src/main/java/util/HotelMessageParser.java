package util;

import com.cleartrip.analytics.Header;
import com.cleartrip.analytics.Hotels;
import io.netty.util.internal.StringUtil;
import model.HotelBook;
import model.HotelSearch;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public class HotelMessageParser {
    public static HotelSearch getHotelSearch(final Hotels.HotelMessage hotelMessage) {
        Hotels.HotelSearch hotelSearch = hotelMessage.getHotelSearch();
        HotelSearch hotelSearchObject = new HotelSearch();
        Header.LogHeader header = hotelMessage.getHeader();
        if (header != null) {
            String searchId;
            String cookie;
            searchId = header.getSearchId();
            cookie = header.getUserCookie();
            hotelSearchObject.setCookie(Optional.ofNullable(cookie));
            hotelSearchObject.setSid(Optional.ofNullable(searchId));
        }

        if (hotelSearch != null) {
            Hotels.HotelItinerary hotelItinerary = hotelSearch.getItinerary();
            if (hotelItinerary != null) {
                String city = hotelItinerary.getCity();
                hotelSearchObject.setCity(Optional.ofNullable(city));
                Long checkin = hotelItinerary.getCheckIn();
                Timestamp checkinTimeStamp = new Timestamp(checkin);
                hotelSearchObject.setCheckIn(checkinTimeStamp);
                Long checkout = hotelItinerary.getCheckOut();
                Timestamp checkoutTimeStamp = new Timestamp(checkout);
                hotelSearchObject.setCheckOut(checkoutTimeStamp);
                if (hotelSearchObject.getCookie().get().equals(StringUtil.EMPTY_STRING)) {
                    hotelSearchObject.setCookie(Optional.of("cookie"));
                }
                if (hotelSearchObject.getSid().get().equals(StringUtil.EMPTY_STRING)) {
                    hotelSearchObject.setSid(Optional.of("sidd"));
                }
                if (hotelSearchObject.getCity().get().equals(StringUtil.EMPTY_STRING)) {
                    hotelSearchObject.setCity(Optional.of("cityy"));
                }
            }
        }
        return hotelSearchObject;
    }

    public static HotelBook getHotelBook(final Hotels.HotelMessage hotelMessage) {
        HotelBook hotelBookObject = new HotelBook();
        Header.LogHeader header = hotelMessage.getHeader();
        if (header != null) {
            String tripId = header.getTripId();
            String cookie = header.getUserCookie();
            hotelBookObject.setCookie(Optional.ofNullable(cookie));
            hotelBookObject.setId(Optional.ofNullable(tripId));
        }
        Hotels.HotelBook hotelBook = hotelMessage.getHotelBook();
        if (hotelBook != null) {
            Hotels.HotelItinerary hotelItinerary = hotelBook.getItinerary();
            if (hotelItinerary != null) {
                Long checkin = hotelItinerary.getCheckIn();
                Timestamp checkinTimeStamp = new Timestamp(checkin);
                hotelBookObject.setCheckIn(checkinTimeStamp);
                Long checkout = hotelItinerary.getCheckOut();
                Timestamp checkoutTimeStamp = new Timestamp(checkout);
                hotelBookObject.setCheckOut(checkoutTimeStamp);

                Hotels.Hotel hotelDetail = hotelItinerary.getHotel();
                String taRating = HotelSortConstants.EMPTY;
                double cashback = 0.00;
                if (hotelDetail != null) {
                    if (hotelDetail.getTaRatingsList() != null && !hotelDetail.getTaRatingsList().isEmpty()) {
                        taRating = hotelDetail.getTaRatingsList().get(0);
                        taRating = (taRating == null) ? HotelSortConstants.EMPTY : taRating.trim();
                    }
                    hotelBookObject.setTaRating(taRating);
                    List<Hotels.Room> roomsList = hotelDetail.getRoomsList();
                    if (roomsList != null) {
                        for (Hotels.Room room : roomsList) {
                            cashback += room.getCashback();
                        }
                    }
                    hotelBookObject.setCashbackFlag(Double.toString(cashback));
                }
                hotelBookObject.setAdults(hotelItinerary.getAdultsPerRoomCount());
                hotelBookObject.setChildren(hotelItinerary.getChildrenPerRoomCount());
                hotelBookObject.setRoomCount(hotelItinerary.getRoomCount());
                hotelBookObject.setStarRating(hotelDetail.getStarRating());
                if (hotelBookObject.getCookie().get().equals(StringUtil.EMPTY_STRING)) {
                    hotelBookObject.setCookie(Optional.of("cookie"));
                }
                if (hotelBookObject.getId().get().equals(HotelSortConstants.EMPTY)) {
                    hotelBookObject.setId(Optional.of("id"));
                }
            }
        }
        return hotelBookObject;
    }
}
