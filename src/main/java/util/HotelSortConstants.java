package util;

public class HotelSortConstants {
    public static final String SEARCH_STREAM_SOURCE_ID = "hotel_search";
    public static final String BOOK_STREAM_SOURCE_ID = "hotel_book";
    public static final String EMPTY = "";
}
