package bolt;

import com.cleartrip.analytics.Hotels;
import com.google.protobuf.InvalidProtocolBufferException;
import model.HotelBook;
import model.HotelSearch;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.HotelMessageParser;
import util.HotelMessageValidator;
import util.HotelSortConstants;

import java.util.Map;

public class HotelBolt extends BaseRichBolt {
    final static Logger LOGGER = LoggerFactory.getLogger(HotelBolt.class);
    private OutputCollector collector;

    @Override
    public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void execute(final Tuple tuple) {
        try {
            Hotels.HotelMessage hotelMessage = Hotels.HotelMessage.parseFrom(tuple.getBinary(0));
            if (HotelMessageValidator.isValid(hotelMessage)) {
                if (hotelMessage.getMessageType() == Hotels.HotelMessage.HotelMessageType.HOTEL_SEARCH) {
                    HotelSearch hotelSearch = HotelMessageParser.getHotelSearch(hotelMessage);
                    collector.emit(HotelSortConstants.SEARCH_STREAM_SOURCE_ID, tuple, new Values(hotelSearch));
                } else if (hotelMessage.getMessageType() == Hotels.HotelMessage.HotelMessageType.HOTEL_BOOK) {
                    HotelBook hotelBook = HotelMessageParser.getHotelBook(hotelMessage);
                    collector.emit(HotelSortConstants.BOOK_STREAM_SOURCE_ID, tuple, new Values(hotelBook));
                }
            } else {
                LOGGER.info("Hotel Message is invalid. Skipping this record.");
            }
        } catch (InvalidProtocolBufferException e) {
            LOGGER.error("Failed to parse the tuple\n" + e.getMessage(), e);
        }
    }

    @Override
    public void cleanup() {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declareStream(HotelSortConstants.SEARCH_STREAM_SOURCE_ID, new Fields("data"));
        outputFieldsDeclarer.declareStream(HotelSortConstants.BOOK_STREAM_SOURCE_ID, new Fields("data"));
    }
}
