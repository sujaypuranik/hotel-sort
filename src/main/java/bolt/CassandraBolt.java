package bolt;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import model.HotelBook;
import model.HotelSearch;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.HotelSortConstants;

import java.util.Map;

public class CassandraBolt extends BaseRichBolt {
    final static Logger LOGGER = LoggerFactory.getLogger(CassandraBolt.class);
    private Session session;
    private PreparedStatement searchPreparedStatement;
    private PreparedStatement bookPreparedStatement;
    private String cassandraHost;
    private final static String KEYSPACE = "ct_ptb_local";

    public CassandraBolt(String cassandraHost) {
        this.cassandraHost = cassandraHost;
    }

    @Override
    public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
        LOGGER.info("Preparing the Cassandra bolt");
        Cluster cluster;
        Cluster.Builder builder = Cluster.builder();
        builder.addContactPoint(cassandraHost);
        cluster = builder.build();
        session = cluster.connect();
        searchPreparedStatement = session.prepare("insert into " + KEYSPACE + ".hotel_search_data (\n" +
                "    cookie, date, sid,\n" +
                "    check_in,\n" +
                "    check_out,\n" +
                "    city)   VALUES (? , dateof(now()) , ? , ?, ?, ?)");
        bookPreparedStatement = session.prepare("insert into " + KEYSPACE + ".hotel_book_data (\n" +
                "    cookie,\n" +
                "    date,\n" +
                "    id,\n" +
                "    adults,\n" +
                "    cashback_flag ,\n" +
                "    check_in ,\n" +
                "    check_out,\n" +
                "    children ,\n" +
                "    days ,\n" +
                "    dx ,\n" +
                "    room_count,\n" +
                "    star_rating,\n" +
                "    ta_ratings) VALUES ( ?, dateof(now())  ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)");
    }

    @Override
    public void execute(Tuple tuple) {
        BoundStatement searchStatement = searchPreparedStatement.bind();
        BoundStatement bookBoundStatement = bookPreparedStatement.bind();

        String sourceStreamId = tuple.getSourceStreamId();
        LOGGER.info("sourceStreamId~ :" + sourceStreamId);

        if (sourceStreamId.equals(HotelSortConstants.SEARCH_STREAM_SOURCE_ID)) {
            HotelSearch hotelSearch = (HotelSearch) tuple.getValueByField("data");
            searchStatement.setString("sid", hotelSearch.getSid().orElse("sid"));
            searchStatement.setString("cookie", hotelSearch.getCookie().orElse("cookie"));
            searchStatement.setString("city", hotelSearch.getCity().orElse("city"));
            if (hotelSearch.getCheckIn() != null) {
                searchStatement.setTimestamp("check_in", hotelSearch.getCheckIn());
            }
            if (hotelSearch.getCheckOut() != null) {
                searchStatement.setTimestamp("check_out", hotelSearch.getCheckOut());
            }
            LOGGER.info("Trying to insert a tuple into hotel_search_data.");
            session.execute(searchStatement);

        } else if (sourceStreamId.equals(HotelSortConstants.BOOK_STREAM_SOURCE_ID)) {
            HotelBook hotelBook = (HotelBook) tuple.getValueByField("data");
            bookBoundStatement.setString("id", hotelBook.getId().orElse("trip-id"));
            bookBoundStatement.setString("cookie", hotelBook.getCookie().orElse("cookie"));
            bookBoundStatement.setTimestamp("check_in", hotelBook.getCheckIn());
            bookBoundStatement.setTimestamp("check_out", hotelBook.getCheckOut());
            bookBoundStatement.setInt("adults", hotelBook.getAdults());
            bookBoundStatement.setInt("children", hotelBook.getChildren());
            bookBoundStatement.setInt("room_count", hotelBook.getRoomCount());
            bookBoundStatement.setString("star_rating", hotelBook.getStarRating());
            bookBoundStatement.setString("ta_ratings", hotelBook.getTaRating());
            bookBoundStatement.setString("cashback_flag", hotelBook.getCashbackFlag());
            bookBoundStatement.setInt("days", 0);
            bookBoundStatement.setInt("dx", 10);
            LOGGER.info("Trying to insert a tuple into hotel_book_data.");
            session.execute(bookBoundStatement);
        }
    }

    @Override
    public void cleanup() {
        session.close();
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }
}
